﻿@using Norbauer.RyanDotNorbauerDotCom.Helpers
@{
    ViewBag.ArticleTitle = "Engineering Blotty Brushes in Photoshop";
    ViewBag.ArticleSuperTitle = "Digital Creative Tools";
}

<section>
    <p>
        In order to make my @Html.ArticleLink("library of Photoshop brush presets", "blotty-ink-pen-brush-preset-library-for-photoshop") for digitally
        inking blotty line art, I had to dive deep into the subject of Photoshop brush tips and presets. I learned a great deal, much of which was
        difficult to cull from various obscure sources on the web, so I thought I'd share here some of the information I learned along the way.
    </p>
    <p>
        I'll start by examining some of the underlying principles of how to build up a brush preset in Photoshop. I'll then set about
        the task of applying that knowledge to creating the effects I'm after: namely, simulating the blotty line work that results from working
        with physical ink pen nibs and crow quill pens.
    </p>

    <p>
        If you already well-aquainted with the finer points of brush presets in PS, feel free to skip directly to
        @Html.ArticleLink("the article where I build up the presets", "engineering-blotty-brushes-part-2-building-the-presets") themselves.
    </p>

    <p>
        I have created a little sample composition to show how one might use these presets. It is admittedly rather hastily made,
        but I'm not an illustrator, so please bear with me; I created it to show how one can combine different brushes from
        the resulting library for various purposes and effects in a single image.
    </p>

    <img alt="Sample composition" src="~/Assets/images/content/articles_blotty-pen-library_sample-composition-small.png">
</section>

<section>
    <h2>References</h2>

    <p>
        Before we get started, a few references. The Andy Warhol Museum in Pittsburgh shows how to achieve
        <a href="http://edu.warhol.org/aract_blot.html">Warhol's original effect</a> in physical media. Also, check out an
        <a href="http://blaine.org/sevenimpossiblethings/?p=1701">interview with Ed Fotheringham</a>, a contemporary illustrator
        who mentions that he uses his own custom Photoshop brushes to achieve his own similar result, which is explicitly based on Warhol's
        technique and style. Also, see <a href="http://coyleart.typepad.com/coyleart/2010/08/photoshop-brushes.html"> this blog post</a>
        by another artist mentioning employing the same approach in PS.
    </p>

    <p>
        So far as I'm aware, no one else on the web specifically shows the technical details of how actually to&nbsp;<em>achieve</em>
        this effect digitally, however, so I had to piece all the information together myself experimentally, employing as best I
        could what I've learned about Photoshop internals from
        <a href="http://www.amazon.com/mn/search/?_encoding=UTF8&amp;camp=1789&amp;creative=390957&amp;field-keywords=deke%20mcclelland&amp;linkCode=ur2&amp;tag=8gj38kd-20&amp;url=search-alias%3Daps">
            Deke McClelland's amazing training resources
        </a>
        (which I highly recommend).
    </p>
</section>

<section>
    <h2>Brushes in Photoshop</h2>

    <p>
        All brushes in Photoshop, whether they appear to create a continuous line or not, work by the repeated application of individual discrete
        daubs of digital paint. Unlike in Illustrator, there are no vectors specifying a well-defined region of brushed area. As with almost everything in Photoshop
        (the exceptions being unrasterized text layers, paths, and vector masks), every time you brush something in the software, you're just recoloring individual pixels to
        varying degrees in the underlying bitmap associated with the active layer.
    </p>

    <p>We can demonstrate this by placing a standard round brush under some experimental scrutiny.</p>

    <p>At a spacing of 25% (100% hardness, 30px size), the line appears continuous and smooth.</p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-1.png">
    </figure>

    <p>Yet when we increase the spacing to 50%, we can see clearly how these strokes are actually composed.</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-2.png">
    </figure>

    <p>At a spacing of 100%, each individual discrete daub of paint is positioned precisely next to the other, without overlapping.</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-3.png">
        <figcaption>
            Note, incidentally, that the S-curve here is just how Photoshop renders thumbnail previews of Brush presets in the
            Brushes panel for illustration purposes, which is where I took these screenshots. The actually drawn line won't have any
            inherent curve unless you draw it that way.
        </figcaption>
    </figure>


    <p>
        For each daub, Photoshop renders out a raster image, which is simply a copy of the base brush tip (which is itself just a
        grayscale raster image). When you're gliding along brushing out lines with a low spacing, these individual daubs visually blend
        together to the extent that their discrete nature is imperceptible. However, under the hood, their composition is entirely of
        discrete little images copied over and over.
    </p>

    <p>
        As a little aside before we move on, let us also note that we can change the roundness of the daubs (30% roundness at 350%
        spacing below).
    </p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-4.png">
    </figure>

    <p>We can also change their angle (90 degrees below)</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-5.png">
    </figure>

    <p>
        Various combinations of these simple options permit the creation of calligraphic line contours. The following is a
        spacing of 25% and a roundness 10%. You can imagine this being composed of lots of tiny overlapping flattened circles as in the
        above more widely-spaced examples.
    </p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-6.png">
    </figure>

    <p>
        Compare this to the same options at a roundness of 100% (i.e., no flattening of the composite circles).
    </p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-7.png">
    </figure>

    <h3>Discrete daubs to our advantage</h3>

    <p>
        When we're using a stroke that is as regular as the simple hard round tip, large spacing values look artificial and take away from the illusion of natural brushing, creating an effect
        that is almost always undesirable.
    </p>

    <p>
        However, the effect created by more widely spaced daubs does provide us with a side-effect that we can use to our advantage
        when creating the appearance of blotty ink lines. If we regard the discrete-daub nature of Photoshop brushes as a source of raw
        "ink blots" (rather than merely visual artifacts of a setting pushed too far) we suddenly have the raw material for an
        infinite well of ink blots.
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-8.png">
    </figure>

    <p>
        In their simplest form, however, these regular round circles don't look at all like anything made by a human hand or pen.
        Our first task, then, is to find a way to introduce some randomness and spontaneity into our blots.
    </p>

    <h4>The <i>Shape Dynamics</i> Options Sub-panel</h4>

    <p>The <i>Shape Dynamics</i> options determine how the shape and size of ink daubs vary within a stroke of the brush.</p>

    <p>
        An important prerequisite bit of vocabulary here is <i>jitter</i>, which is Adobe's term for random variation introduced into the
        values of parameters of the brush tip daubs as they are laid down in the course of a brush stroke (including parameters affecting the
        individual daubs themselves and their spacing and count within the overall line.)
    </p>

    <h5>Size Jitter</h5>
    <p>
        <i>Size Jitter </i>specifies what amount of variation is to be introduced into the size of the individual daubs that are laid
        down, and what its source is to be.
    </p>

    <p>We can choose a totally random source, a user input source, of some combination of the two.</p>

    <p>
        Still working with a round hard brush of 30px and spacing of 100%, here is what our base brush looks like without any
        dynamics introduced.
    </p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-9.png">
    </figure>

    <p>Here is if we leave <i>Size Jitter</i> at 0% but select <i>Pen Pressure</i>from the <i>Control </i>option drop-down.</p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-10.png">
        <figcaption>
            The thumbnail above is representing light pressure on both ends and heavy in the middle, so we can see that variation in the
            size of the daubs is entirely controlled by pen pressure.
        </figcaption>
    </figure>

    <p>Here is what things look like if we set <i>Size Jitter</i> to 100% but select <i>Off</i> from the <i>Control </i>option drop-down.</p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-11.png">
    </figure>

    <p>The size of the daubs will vary randomly, irrespective of tablet pressure.</p>

    <p>Below is if we leave set <i>Size Jitter </i>to 100% and select <i>Pen Pressure </i>from the <i>Control </i>option drop-down.</p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-12.png">
    </figure>

    <p>
        This thumbnail is representing light pressure on both ends and heavy in the middle, so we can see that we get random
        variation in daub size, but influenced to some extent by pen pressure.
    </p>

    <h5>Roundness Jitter</h5>

    <p>
        This specifies what amount of variation is to be introduced into the roundness of the individual daubs that are
        laid down, and what its source is to be.
    </p>

    <p>Still working with a round hard brush of 30px and spacing of 100%, here is our base brush</p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-13.png">
    </figure>

    <p><i>Control </i>set to <i>Pen Pressure</i>, 0% <i>Roundness Jitter</i></p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-14.png">
    </figure>

    <p><i>Control </i>set to Off, 100% <i>Roundness Jitter</i></p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-15.png">
    </figure>

    <p><i>Control </i>set to <i>Pen Pressure</i>, 100% <i>Roundness Jitter</i></p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-16.png">
    </figure>

    <h4>The <i>Scattering</i> Options Sub-panel</h4>
    <p>This panel controls the number and distribution of the daubs within the stroke.</p>

    <h5>Scatter value</h5>
    <p>How far the scattering takes the daubs away from their axis (or both axes) of scatter.</p>

    <p>100%</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-17.png">
    </figure>

    <p>200%</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-18.png">
    </figure>

    <p>300%</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-19.png">
    </figure>

    <p>
        Note that the <i>Both Axes</i> checkbox (which is on by default) causes scattering in both directions (when unchecked, the
        single-axis scatter employed is the one perpendicular to the stroke path).
    </p>

    <p>100%, <i>Both Axes</i> off</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-20.png">
    </figure>


    <h5>Count</h5>
    <p>Specifies the number of paint daubs laid down at each spacing interval.</p>

    <p><i>Count </i>4, <i>Spacing</i> 50%, <i>Scatter </i>200%</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-21.png">
    </figure>


    <p><i>Count</i> 4, <i>Spacing</i> 100%, <i>Scatter</i> 200%</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-22.png">
    </figure>


    <p><i>Count</i> 4, <i>Spacing</i> 200%, <i>Scatter </i>200%</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-23.png">
    </figure>


    <p><i>Count</i> 16, <i>Spacing </i>100%, <i>Scatter </i>200%</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-24.png">
    </figure>

    <p>
        Note that with <i>Scatter</i> set to 0, the <i>Count </i>value basically becomes irrelevant, because whatever number of daubs are
        specified in the <i>Count </i>value will all be laid on directly top of each other for each interval, looking the same as if there were
        only 1.
    </p>

    <p><i>Scatter </i>0%, <i>Count </i>2</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-25.png">
    </figure>


    <p><i>Scatter</i> 100%, <i>Count </i>2</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-26.png">
    </figure>


    <h5>Count jitter</h5>
    <p>Allow us to introduce randomness into the <i>Count </i>value, similar to all the jitter scenarios described above.</p>

    <p><i>Scatter </i>200%, <i>Count </i>2, <i>Count Jitter </i>0%</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-27.png">
    </figure>

    <p><i>Scatter </i>200%, <i>Count </i>2,<i> Count Jitter </i>100%</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-28.png">
        <figcaption>Gives a non-uniform distribution of the daubs from one interval to the next.</figcaption>
    </figure>

    <h4>Dual Brush</h4>
    <p>
        This is a fascinating (and slightly complicated to grasp) option. In the simplest sense, it takes two brush tips and
        combines them to make a single brush tip.
    </p>

    <p>
        More precisely, and assuming you're using the (most common) <i>Multiply </i>mode, the dual brush option takes the brush tip that
        was set in the original <i>Brush Tip Shape </i>and then uses its outline as a mask in which to paint a second brush tip's line:
        only those areas where the two brush tips intersect will color the pixels below them.
    </p>

    <p>So, using a <i>Multiply </i>blend mode and our original brush tip shape (still 30px, <i>Spacing </i>100%), which looks like this:</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-29.png">
    </figure>

    <p>
        If we add to this a 25px soft round brush tip as our <i>Dual Brush</i> tip, we can see that the smooth soft line painted by that
        <i>Dual Brush</i> tip is being masked away in the areas where it paints beyond the (effective) mask of the original parent brush tip.
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-pre-1.png">
    </figure>


    <h5>Mode</h5>
    <p>
        The dual brush <i>Mode </i>option is rather perplexing. As Deke McClelland puts it in his
        <a href="http://www.lynda.com/tutorial/61019">PS One-on-One Mastery Course</a>, "All I'm going to tell you about
        <i>Mode</i>: <i>Mode </i>makes no sense," which is to say that all of the traditional concepts around blend modes
        are inverted or muddled. For example, <i>Multiply </i>and <i>Darken </i>end up lightening, and what is normally a lightening
        blend mode, <i>Color Dodge, </i>has a darkening effect. We also have the modes <i>Height </i>and <i>Linear Height</i>, neither
        of which exist as part of the normal PS blend modes. I've done a lot of research and poking around the web (and experimentation
        on my own) looking for implementation details of how the two brushes tips interact with the <i>Mode</i> option when using a
        <i>Dual Brush</i>, but I have yet to work out the details. If you happen to stumble across any leads, please
        <a href="http://ryan.norbauer.com/contact/">contact me</a> and I'll happily post the details here. In the meantime, it's pretty
        much a matter of cycling through the options and seeing what looks good with your particular combination of other settings.
        I'll continue to look for more rigorous details of the underlying engineering and will update this document if I ever encounter any.
    </p>

    <p>At any rate, here are the same settings as above but with<i>Hard Mix </i>applied</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-30.png">
    </figure>
    <p>
        Obviously, some sort of threhold value is being applied and pretty much all of both brush strokes are coming through, with a
        bit of rough edges (as we would expect from a <i>Hard Mix</i> under normal blending).
    </p>

    <h5>Size, Spacing, Scatter, and Count</h5>
    <p>Controls how these parameters are set for the <i>Dual Brush </i>tip stroke.</p>

    <h4>The <i>Transfer </i>Options Sub-panel</h4>
    <p>These options determine how opacity and flow change (or don't) over the course of a stroke.</p>

    <h5>Opacity Jitter</h5>

    <p>Here is at 50%</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-31.png">
    </figure>

    <p>Or we can set the control to <i>Pen Pressure</i> (et al)</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-32.png">
    </figure>


    <h5>Flow</h5>
    <p>Same goes for flow.</p>

    <p>Here is 100% jitter</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-33.png">
    </figure>


    <p>And <i>Pen Pressure </i>control</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-thumbnail-34.png">
    </figure>
</section>

<section>
    <h2>Summary</h2>
    <p>Those are the important options for us to understand in order to build a set of blotty brushes, @Html.ArticleLink("so let's get to it", "engineering-blotty-brushes-part-2-building-the-presets").</p>
</section>