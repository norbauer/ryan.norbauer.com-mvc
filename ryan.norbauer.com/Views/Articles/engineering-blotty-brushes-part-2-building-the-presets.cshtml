﻿@using Norbauer.RyanDotNorbauerDotCom.Helpers
@{
    ViewBag.ArticleTitle = "Engineering Blotty Brushes in Photoshop: Building the Presets";
    ViewBag.ArticleSuperTitle = "Digital Creative Tools";
}
<section>
    <p>
        In @Html.ArticleLink("Part I", "engineering-blotty-brushes-part-1-brushes-in-photoshop") of the discussion of how to build up a
        @Html.ArticleLink("library of blotty ink pen brush presets for Photoshop", "blotty-ink-pen-brush-preset-library-for-photoshop"),
        we looked at the fundamentals of PS brushes, with a particular eye towards simulating ink blots. Now that we've covered the fundamentals,
        we can set about actually building up the brushes we want from the simple stock brush tips that ship with the software.
    </p>
</section>

<section>
    <h2>A Line with Visible Discrete Blots</h2>
    <p>
        We'll start with <i>Size 9 Hard Round</i> from the <i>Basic Brushes</i> set of presets that ships with Photoshop.
        The size isn't too important; I just have found this to be a good size when inking on a roughly screen-sized canvas (at 100% zoom).
    </p>

    <p>
        We can turn all our other panel options off and build up from there (with the exception of smoothing, which just adds a bit of path
        smoothing to lines drawn with the brush tool.)
    </p>


    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-pre-2.png">
    </figure>


    <p>Now, let's get some blots to start with, increasing <i>Spacing</i> to 100%.</p>


    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-pre-3.png">
    </figure>

    <p>
        Now, let's go into the <i>Shape Dynamics</i> sub-panel. For naturalistic lines, we'll want some good <i>Size Jitter</i>, as
        if the pen nib is jittering and sputtering along the physical surface of the paper, causing variant blot sizes.
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-pre-4.png">
    </figure>

    <p>
        Let's also add in some <i>Pen Pressure </i>control, so we can taper the effect. This will particularly help the ends of our lines
        look good, assuming we start in with gentle pressure and press harder in the middle of strokes (a generally good practice when
        working for a blotty effect with presets like the ones we're building up here).
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-pre-5.png">
    </figure>

    <p>
        Notice that things are already starting to look pretty good, but we can do better. For one thing, our circles look too
        regular for a naturalistic effect. We can add in some <i>Roundness Jitter</i> (and <i>Pen Pressure </i>control for good measure, though the
        effect will be barely noticeable with these settings).
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-pre-6.png">
    </figure>

    <p>Notice, however, that this creates some overly flattened portions of the line, which make the whole thing look unnatural.</p>

    <p>
        One way to fix this would be to go back to the <i>Brush Tip Shape</i> sub-panel and decrease the spacing, so the various circles
        overlap more and we can't see the individual flattened ellipses.
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-pre-7.png">
    </figure>

    <p>
        We'll look at situations like this in the next preset we build (where we'll be looking for a more continuous blotty
        line). For this first example, though, we want something more discretely blotty (with more visible individual blots and a less
        continuous line).
    </p>

    <p>
        So let's set our <i>Spacing </i>back to 100% in the <i>Brush Tip Shape</i> panel and simply set a <i>Minimum Roundness </i>option in the
        <i>Shape Dynamics</i> options sub-panel. In this case, I thought a value of 60% worked well.
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-pre-8.png">
    </figure>


    <p>Now, we're getting a nicely blotty looking line with some plausibly natural looking variation in the blots (both in their size and spacing).</p>

    <p>
        At this point, our line looks a little too cleanly like a straight line (too computer-generated), because all of our dots
        are so precisely following the line path. If we've got ink blots flying all over the place from a quivering quill tip, they
        should be doing just that, which brings us to the <i>Scatter </i>sub-panel.
    </p>

    <p>
        <i>Scatter </i>is an easy option to overdo. Let's go with something like just 40% and again let <i>Pen Pressure </i>influence things.
        Leave <i>Both Axes </i>turned on, because if we have a real-world quill splattering ink, there is nothing that would confine these
        spatters to a single axis.
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-pre-9.png">
    </figure>


    <p>
        The effect here is subtle (barely visible in the thumbnail), but these little subtleties all piled on top of each other are
        what ultimately shall make for a credible effect.
    </p>

    <p>
        Given that our <i>Count</i> value is set to 1, any <i>Count Jitter</i> value below 100% will have no effect (we can think of this as
        being because randomly varying between 1 and 1 is always 1).
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-1.png">
    </figure>

    <p>
        However, if we set our <i>Count Jitter </i>value to 100%, we'll vary between 1 and 0, meaning that it'll randomly switch between
        having one daub per interval and zero daubs, which creates an effect of a pen that skips.
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-2.png">
    </figure>


    <p>
        This is an interesting effect, but it's not what we're going for here, so let's just set <i>Count Jitter </i>to 0 and
        discount that option for this particular preset.
    </p>


    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-3.png">
    </figure>

    <h3>The all-important Hard Mix hack</h3>

    <p>
        We now have the broad profile of an interestingly varied pen line. However, the composite shapes are much too uniform and
        regular. We can clearly make out very regular ellipses, which looks entirely synthetic and much too vector-like, especially
        since the ellipses are being given such nicely softened/anti-aliased edges.
    </p>

    <p>
        So the next thing we want to do—and, in truth, here (in my opinion) is the main trick of creating truly credibly blotty pen
        lines—is to figure out how to apply one of those wacky texture blend modes we saw above in the discussion of the <i>Dual Brush</i> options:
        namely, <i>Hard Mix</i>. <i>Hard Mix</i> is going to roughen up the edges by reducing the number of gray shades that come through the brush.
    </p>

    <p>
        So, yes, this next step involves a bit of a hack. We want to apply <i>Hard Mix </i>to our brush outline, but a blending mode only makes sense as
        an option when used to combine <em>two</em> images, so it can only be applied by either pretending we're going to add a texture or a
        second (<i>Dual</i>) brush to our original brush tip. I'll show you both techniques. But bear in mind that we actually don't really
        care about creating either a <i>Texture </i>or <i>Dual Brush </i>effect at this point; we're just using those two panels as a way of
        getting at the <i>Hard Mix</i> mode and applying it to our brush tip (and, not to be too pedantic, but
        in this sense the <i>Mode</i> option here is thus actually acting more like a filter than like a blending mode).
    </p>

    <p>
        To get at <i>Hard Mix </i>via the <i>Texture </i>options sub-panel, select any random texture you'd like from the drop-down menu. By
        reducing the <i>Depth</i> value to 0%, we entirely remove the texture's contribution to the brush (it's the same as entirely turning off
        the <i>Texture </i>options, as best as I can tell), but we still have the <i>Mode</i> option available to us.
    </p>


    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-4.png">
    </figure>

    <p>Notice how this roughens up the edges of our ellipses and makes for a much more, well, blotty effect.</p>

    <p>We can achieve roughly the same thing by using a <i>Dual Brush</i> under the <i>Hard Mix</i> mode. I chose a 1px brush tip.</p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-5.png">
    </figure>

    <p>Note that this looks nearly identical to the <i>Texture</i>-based approach above.</p>

    <p>
        That 1px line coming from the second (<i>Dual</i>) brush may show through the end mix a bit, creating a partially-visible fine
        underlying continuous line connecting the blots, so I'm going with this approach in my presets, but fundamentally I think this
        approach is very similar to the <i>Texture</i> one mentioned above (if not identical). Far more contribution to our overall effect is
        coming from the <i>Hard Mix</i> mode than from whatever this tiny second <i>Dual Brush</i> tip is doing.
    </p>

    <p>If you do want to push the borders of your blobs outward even more, however, you can choose a larger brush tip.</p>

    <p>Here is a 7px soft round</p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-6.png">
    </figure>


    <p>And a 39px angled elliptical tip</p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-7.png">
    </figure>

    <p>
        Ultimately, the <i>Dual Brush </i>options are going to give you more control and variations (should you need/want them), so I
        recommend those over the <i>Texture </i>strategy. But just bear in mind that you shouldn't try too hard to wrap your head around the
        wonky <i>Dual Brush </i>modes and options, because the most important thing we're getting out of this panel right now is the reduction of edge
        softening furnished by the application of the <i>Hard Mix </i>mode, which acts somewhat independently of the way in which the two
        brush tips are combined in <i>Dual Brush</i>.
    </p>

    <p>But, as I say, for my purposes here, I'm going to keep it (relatively) conceptually simple and go with a 1px dual brush.</p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-8.png">
    </figure>

    <p>And so we have our first crack at a plausible looking brush.</p>

    <p>I'll examine this brush preset by taking a light bulb color background and inking some lines on top.</p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_bulb-background.png">
    </figure>

    <p>
        But before testing out any preset, we should make sure to ink with the <i>Tablet pressure controls opacity</i> override button in the brush options
        panel turned off (so we ink only with pure black and don't get any grays as our pressure varies).
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_pressure-opacity-override-option.png">
        <figcaption>
            You can always experiment with turning this option on at a later point; it creates a very wet/translucent ink effect, which I don't
            personally find very appealing but which could be useful for certain applications. To duplicate the ink effects as I've
            created them above, you'll certainly want to work with it off.
        </figcaption>
    </figure>

    <p>In any case, we end up with the following, which in my library I call <i>Blotty Pens - Discrete Line.</i>
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_specimen-discrete-line-hand.png">
        <figcaption>
            Note that this, as with all the examples below, was slightly downsampled; you're viewing it at approximately 67% original
            size. Blotty brush drawings generally look best when drawn large and then downsampled a bit.
        </figcaption>
    </figure>

    <p>
        The above specimen was hand drawn using my stylus and (Wacom Intuous 5) tablet. Next, so we have uniform comparison for our
        different presets to follow, I'll apply the brush along a vector path.
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_specimen-discrete-line-path.png">
    </figure>
</section>    
    
<section>
    <h2>A blobbier discrete blots line</h2>
    <p>
        To make our line blobbier (as if the quill were more laden with ink), we can simply reduce the <i>Roundness Jitter </i>to 0% and
        let the roundness of the blobs vary entirely with <i>Pen Pressure</i>.
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-9.png">
    </figure>


    <p>The result is as follows, titled in my library <i>Blotty Pens - Discrete Line(Blobby).</i></p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_specimen-discrete-line-blobby-hand.png">
    </figure>

    <p>And here it is along the standard path</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_specimen-discrete-line-blobby-path.png">
    </figure>
</section>

<section>
    <h2>Blobby Wet</h2>
    <p>
        We can make the quill seem even wetter and more laden with ink by taking <i>Blotty Pens - Discrete Line(Blobby)</i> and selecting a
        20px soft round <i>Dual Brush</i>.
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-10.png">
    </figure>

    <p>The result is as follows, titled <i>Blotty Pens - Discrete Line (Blobby, Wet)</i> in my presets library.</p>

    <p>Here is my hand-drawn version</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_specimen-discrete-line-blobby-wet-hand.png">
    </figure>

    <p>Here is the version using the standard paths:</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_specimen-discrete-line-blobby-wet-path.png">
    </figure>
</section>


<section>
    <h2>Round and Regular Dotty Blots</h2>
    <p>
        What if we wanted to accent some details with a series of fully discrete round blots, as if we had touched the crow quill
        down and lifted it over and over again, creating a line of separate (though obviously not identically sized) dots? Easy
        enough.
    </p>

    <p>Let's go back to the settings from <i>Blotty Pens - Discrete Line</i> as a base and modify from there.</p>

    <p>
        We can turn off the <i>Dual Brush</i> options (because we want a regular shape this time, with smooth anti-aliasing to emphasize
        the regular contours). <i>Scattering </i>can remain the same. And the only thing we'll change in <i>Shape Dynamics </i>is to turn off the
        variation in roundness, so we get perfectly round daubs and bump up the <i>Minimum Diameter </i>(in order to avoid getting blots so
        small they almost look like a continuous line).
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-11.png">
    </figure>


    <p>Now in the base <i>Brush Tip Shape</i>, let's increase the spacing and decrease the size a bit.</p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-12.png">
    </figure>


    <p>In my library, I'm calling this preset <i>Blotty Pens - Dotty Blots.</i></p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_specimen-dotty-blots-hand.png">
    </figure>

    <p>Here is the version along the standard path</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_specimen-dotty-blots-path.png">
    </figure>
</section>

<section>
    <h2>Continuous line </h2>
    <p>
        Now let's make more of a continuous blotty line, where the individual ink blots are less visible. This is actually more
        like what a slightly erratic inking pattern with a pen nib actually looks like. For example, I made the following image
        with a real-world dip pen and ink.
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_43folders-hipster-pda.jpg">
    </figure>

    <p>
        Notice that there aren't too many visible discrete blots. It's a slightly different style, less like the Warhol technique
        (which, incidentally, involves physically blotting the wet ink with a second absorbent sheet of paper after it has been laid down with a pen).
        This style is more like more like direct inking on paper.
    </p>

    <p>
        To replicate it, we need to move our daubs/blots closer together. We'll still have a great deal of blotty irregularity
        in our line from all the work we've done above, but there will be little (to no) white space between brush daubs.
    </p>

    <p>Once again, let's go back to the settings from <i>Blotty Pens - Discrete Line</i> as a base and modify from there.</p>

    <p>
        We should start by reducing the <i>Spacing </i>value under <i>Brush Tip Shape</i> to 1%. I like to take the default base tip size down to
        7px since this brush will naturally draw darker (and thus stronger) looking lines; you can always adjust the size at any
        time, as with any brush, using the right and left bracket shortcut keys.
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-13.png">
    </figure>


    <p>
        In <i>Shape Dynamics</i>, we can crank our <i>Size Jitter</i> all the way up, since wildly varying sizes are less obtrusive
        when they're not separately spaced apart and visible individually. This lends the maximum amount of blotty variability to the line edge.
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-14.png">
    </figure>

    <p>The contribution of <i>Scattering </i>to a continuous line is minimal, so we can turn that off entirely.</p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-15.png">
    </figure>

    <p>And for now the <i>Dual Brush</i> settings can remain as we set them originally for our discrete-blots line.</p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-16.png">
    </figure>

    <p>I'm calling this preset in the library <i>Blotty Pens - Continuous Line</i>.</p>

    <p>Here is the haphazard hand version</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_specimen-continuous-line-hand.png">
    </figure>

    <p>Here is the standard vector path version</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_specimen-continuous-line-path.png">
    </figure>
</section>

<section>
    <h2>Continuous Line (Skippy)</h2>
    <p>
        Building on the <i>Continuous Line</i> preset from above, we can add some skipping into the line by spacing
        out some <i>Dual Brush</i> daubs in the line, as follows.
    </p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-17.png">
    </figure>

    <p>This one in the library is <i>Blotty Pen - Continuous Line (Skippy)</i>.</p>

    <p>By hand</p>

    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_specimen-continuous-line-skippy-hand.png">
    </figure>


    <p>The vector path</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_specimen-continuous-line-skippy-path.png">
    </figure>
</section>

<section>
    <h2>Continuous line (skippy, absorbent paper)</h2>
    <p>
        For my last, final, and best trick, I present my favorite and most naturalistic looking of the presets. It creates the effect
        of a very skippy pen dragging along a very absorbent piece of paper.
    </p>

    <p>We'll use a 50% spacing this time</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-18.png">
    </figure>


    <p>
        We'll cut down the <i>Roundness Jitter</i> a bit but otherwise leave
        the <i>Shape Dynamics </i>as in the other continuous line examples.
    </p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-19.png">
    </figure>

    <p>
        And here the secret lies in selecting an unusual dual brush:
        <i>Hard Elliptical 39</i>.
    </p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_brush-panel-screencapture-20.png">
    </figure>

    <p>
        Also, by spacing it out by 400%, it means that we'll only get one of these large elliptical daubs being hard-mixed with our
        regular line for every four intervals (you can just make out about three of them in the thumbnail above). In these spaces of
        overlap, it looks like we have a big blot that has bled into the paper.
    </p>

    <p>In the library, this is <i>Blotty Line - Continuous (Skippy, Absorbent Paper)</i>.</p>

    <p>Here is the hand-drawn version</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_specimen-continuous-line-skippy-absorbent-paper-hand.png">
    </figure>

    <p>And the standard vector</p>
    <figure class="screenshot_elucidating_preceding_paragraph_in_the_context_of_a_tutorial">
        <img src="~/Assets/images/content/articles_blotty-pen-library_specimen-continuous-line-skippy-absorbent-paper-path.png">
    </figure>
</section>

<section>
    <h2>There you are.</h2>
    <p>
        Now that you understand the underlying principles involved in making the presets in my @Html.ArticleLink("library", "blotty-ink-pen-brush-preset-library-for-photoshop")
        (rather than just downloading and using them), you can go off and confidently create your own or modify the existing ones. Have fun.
        And if you make something pretty with it, do @Html.PageLink("let me know", "contact").
    </p>
</section>