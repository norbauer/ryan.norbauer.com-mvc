﻿@using Norbauer.RyanDotNorbauerDotCom.Helpers
@{
    ViewBag.ArticleSuperTitle = "Portfolio";
    ViewBag.ArticleTitle = "Business";
    ViewBag.PageTitleForSearchEnginesAndBrowserTabs = "Business Portfolio";
}

<section>
    @Image.WithoutLightbox("pages_business-portfolio_norbauer-inc-illustration.png", altText: "Norbauer Inc illustration by Jessica Hische")

    <p>
        At no point in my life until I stumbled into founding my own companies did I ever desire to be a man of business—and
        even then I remained ambivalent. But at some point early on in life I learned that it's irksome to work for people who are
        dumber than you are, and that doing things yourself is frequently the only way to make things happen in the world. Thus the idea of
        entrepreneurship began to have an appeal.
    </p>

    <p>
        I suppose also that, having originally come coming from an academic background, I was simply insecure about and intimidated by the supposed "real world"
        of work and money-making, and I wanted to prove to myself that it was a domain in which I could be productive—whether or not that was something
        I inherently valued very much.
    </p>

    <p>
        At any rate, I started three companies, ran them until I stopped learning new things from the work, and then sold each one. Though they were all in varying
        ways sources of frustration and disappointment, I'll concede that they were all far more successful than I could ever have hoped or imagined at the outset
        of each enterprise, and from each of them I was able to learn about and do things that would otherwise never have been possible.
    </p>
</section>

<section>
    <h2>Lovetastic / Scene404</h2>

    @Image.WithLightbox("pages_business-portfolio_lovetastic.png", altText: "Lovetastic illustration")

    <p class="information-supplemental-to-header">Acquired by: OKCupid (now Match.com)</p>
    
    <p>@Html.PageLink("Screen captures and selected press clippings.", "business-portfolio-lovetastic-details").</p>

    <p>
        In many ways, I'm probably most proud of Lovetastic, if for no other reason than that it was a project it affected more people than anything else I've ever done
        (or perhaps am likely ever to do). It also happens to be where I met my husband Alan.
    </p>

    <p>
        The idea, which I had around 2004, was simple: to build an online community for guys who like guys but who feel like we don't quite fit in with mainstream gay culture.
        Originally, I called the site Scene404 (a nerdy reference to the "gay scene" and server error code returned when you try to go to a location that doesn't exist). I had
        just come back from an exciting year working in the British Parliament and found myself back in the ivory tower, very much missing feeling connected to the real world.
        So after a few weeks I cashed in my academic chips (which actually turn out to be worthless), and set off on my own to build a web start-up. The only trouble was that
        I had never built a web application in my life, or practically any production-ready software for that matter—and I had no idea how to run a business. So I sat down on Day 1,
        opened up a book on SQL and went to town, endeavoring to study software engineering as assiduously as I had my scholarly subjects. After about six solitary months of
        (mostly nocturnal) 14-hour days behind my Linux terminal, Scene404 went live.
    </p>

    <p>
        Our membership grew steadily thanks to early positive feedback on sites like GayGeeks.org and due to a series of early podcasts I did interviewing interesting stereotype-busting
        gay men. After running a few ads in <i>The Harvard Gay and Lesbian Review</i>, I managed to get some early attention from a small but influential (and thoughtful) crowd of
        gay men. Within a few months, I had been approached for investment in the company. I eventually accepted an offer to sell a portion of my shares in order to raise funds
        to undertake a major promotion and advertising campaign in hopes of launching the project into national attention.
    </p>

    <p>
        In this manner I went from coding a speculative website from my father's house in an obscure part of West Virginia (not that there are non-obscure parts), within a year I
        suddenly found myself running a company with an office in Manhattan, hiring a fancy Madison Avenue PR firm that also represented the likes of BMW, Activision, Disney,
        and Bloomberg, doing interviews for major national newspapers, and celebrating our deal closing at the Four Seasons (the restaurant in the Seagrams Building, not the
        hotel)—heady stuff for a 24-year-old from the Appalachian sticks. For me, the excitement of this era is perfectly encapsulated in
        <a href="http://twit.tv/show/netnight-amber-and-leo/36">this nervous interview</a> I did with Leo Laporte (a journalist, formerly of TechTV, and hero of mine from adolescence.)
    </p>

    <p>
        Part of the investment in the company involved a major rebranding and trademarking effort to "Lovetastic," and it gave me resources to oversee a comprehensive visual redesign
        and a ground-up rewrite of the software, along with an overhaul of our server infrastructure. Being able to hire a team of developers made it possible to do all sorts of interesting
        things, including being one of the first commercial production websites to use Ruby on Rails. We were also among the first sites to use Amazon S3, Mechanical Turk, and a
        cloud hosting solution. My early work and involvement in the Ruby on Rails community is what would lead me to found my second company—more on that below.
    </p>

    <p>
        Within the first few weeks of launching the new site, Lovetastic's active membership quickly skyrocketed into the tens of thousands, and the site steadily grew over the
        following few years. However, the site never quite became the site with millions of users that we had hoped for, and the distractions of running another company took Lovetastic
        away from the center of my attention.
    </p>

    <p>
        In 2010, <a href="http://www.okcupid.com/lovetastic">Lovetastic was purchased by OKCupid</a>. I couldn't be more proud of this, as
        <a href="http://www.bbc.com/news/technology-26830383">OKCupid has been at the forefront of advocacy for the gay community</a> and has
        always treated gay men with the respect that our own "community" dating sites have never deemed us worthy to receive.
        Now, when people come to OKCupid (one of the highest traffic sites on the web) and say they're gay, they see the mascot illustrator that I originally
        commissioned for Scene404, along with a number of suggested features that we at Lovetastic made specifically at the request of the OKCupid co-founders
        in order to help make the site more friendly to gay men. I still think all this is pretty cool.
    </p>

    <p>
        Running Lovetastic was admittedly an exciting, stressful, and at times frustrating, enterprise. But I owe much that is my life to it, and I couldn't be happier
        to have thrown off my academic career trajectory to pursue my wacky business idea in spite of the baffled protests of nearly everyone around me at the time.
    </p>
</section>

<section>
    <h2>Norbauer Inc</h2>

    @Image.WithLightbox("pages_business-portfolio_norbauer-inc-company-masthead.png")
    
    <p class="information-supplemental-to-header">Acquired by: Jonathan Dance (now of Heroku) and Tom Piekos.</p>

    <p>
        My first business, Lovetastic, was one of the first large commercial sites to go into production using Ruby on Rails. As a result of my early involvement
        in and advocacy for the Rails community, I achieved some small measure of prominence in the Rails community and people would frequently
        come to me asking for consulting work. When the proposed work was abnormally interesting or lucrative, I would reluctantly take on such
        gigs here and there to keep pushing my technical skills. Demand quickly picked up such that I was spending as much time fielding consulting
        inquiries as managing Lovetastic development. Around the same time, I was reading a number of books that made me interested in the idea of
        building and scaling a professional services firm. So, in 2005, I decided to start building an engineering team and to embark on a new
        business experiment.
    </p>
    <p>
        By 2010, we were a team of over twenty software architects, engineers, and designers with offices in Boston, New York, and New Delhi
        and clients all over the world. We helped write the code of the most popular and sophisticated open-source web framework at the time
        (Ruby on Rails), with several core contributors on our team (back in the pre-Github days, when that was saying a lot more than it does today).
        We enjoyed a distinguished reputation in the open-source world and had the good fortune to build systems for organizations like Microsoft, AMD,
        Yale University, tech publisher IDG, and a number of tech start-ups and international corporations. We built sites that served millions of
        pageviews a day and apps that are still are moving money and goods around the world in multiple currencies and languages every day.
    </p>
    <p>
        In my capacity running the company, I was frequently interviewed and cited by the likes of <i>Wired</i>, <i>Forbes</i>,
        <i>The Boston Globe</i>, so running the company also appealed to my stupid insecure vanity. And for some reason I was always
        particularly proud of the fact that for the entire time I was running the company if you typed "Rails consulting"
        into Google, we were always the first result.
    </p>

    <p>
        As the company and scale of the company grew, however, I started to become less involved in software architecture and ended up spending
        most of my time dealing with problem clients (my favorite was one from whom we were unable to collect an outstanding $100,000 invoice
        for services rendered because he ended up in Federal prison). So when I was made a compelling offer to sell the company to a consortium of
        accomplished, Rails engineers, I decided it was time to give up on life and go into the eccentric pseudo-"retirement" that now occupies my days.
    </p>
</section>

<section>
    <h2>RubyRags</h2>

    @Image.WithoutLightbox("pages_business-portfolio_rubyrags.png", altText: "RubyRags masthead")

    <p class="information-supplemental-to-header">Acquired by: LittleLines</p>

    <p>
        I created this apparel company for a number of reasons, but mostly because it gave me a tax-deductible excuse to hire illustrators and learn all about
        screen-printing and mail-order fulfillment/logistics. It was <a href="http://www.norbauer.com/rails-consulting/notes/rubyrags-a-littlelines-project.html">purchased</a> by
        the Rails design and consulting firm Littlelines, and is still very much <a href="http://www.rubyrags.com/">in operation.</a>
    </p>

    <p>RubyRags enjoyed the distinction of producing and selling the first ever Github t-shirts, which as I recall simply read "Fork you."</p>
</section>