﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Norbauer.RyanDotNorbauerDotCom.Helpers
{
    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString RemoveHtmlTags(this HtmlHelper helper, String textThatMayContainHtmlTags)
        {
            var array = new Char[textThatMayContainHtmlTags.Length];
            Int32 arrayIndex = 0;
            Boolean inside = false;

            foreach (Char character in textThatMayContainHtmlTags)
            {
                if (character == '<')
                {
                    inside = true;
                    continue;
                }
                if (character == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = character;
                    arrayIndex++;
                }
            }
            var strippedString = new String(array, 0, arrayIndex);
            return MvcHtmlString.Create(strippedString);
        }
    }

    
}