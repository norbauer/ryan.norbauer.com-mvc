﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace Norbauer.RyanDotNorbauerDotCom.Helpers
{
    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString ArticleLink(this HtmlHelper helper, String linkText, String slug)
        {
            return helper.RouteLink(linkText, "Article", new { Slug = slug});
        }

        public static MvcHtmlString PageLink(this HtmlHelper helper, String linkText, String slug)
        {
            return helper.RouteLink(linkText, "Page", new { Slug = slug });
        }
    }

    public static partial class UrlHelperExtensions
    {
        public static String ArticleUrl(this UrlHelper helper, String slug)
        {
            return helper.RouteUrl("Article", new { Slug = slug });
        }

        public static String PageUrl(this UrlHelper helper, String slug)
        {
            return helper.RouteUrl("Page", new { Slug = slug });
        }
    }
}