﻿using System;
using System.Web.Configuration;
using System.Web.Mvc;
using JetBrains.Annotations;

namespace Norbauer.RyanDotNorbauerDotCom.Helpers
{
    public static partial class UrlExtensions
    {
        // relative Path should be relative to the site root. ~/Assets/images/whatever.jpg is a fine example.
        public static String Asset(this UrlHelper helper, [PathReference] String relativePath)
        {
            var cdnRoot = WebConfigurationManager.AppSettings["cdnRoot"];
            if (String.IsNullOrEmpty(cdnRoot))
            {
                return UrlHelper.GenerateContentUrl(relativePath, helper.RequestContext.HttpContext);
            }
            else
            {
                String rootedPathToAssets = "~/Assets";
                if (relativePath.StartsWith(rootedPathToAssets))
                    relativePath = relativePath.Replace(rootedPathToAssets, String.Empty);

                if (cdnRoot.EndsWith("/"))
                    cdnRoot = cdnRoot.TrimEnd('/');

                if (!relativePath.StartsWith("/"))
                    relativePath = "/" + relativePath;

                return cdnRoot + relativePath;
            }
        }

    }
}