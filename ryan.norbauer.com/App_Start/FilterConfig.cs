﻿using System.Web;
using System.Web.Mvc;

namespace Norbauer.RyanDotNorbauerDotCom
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
