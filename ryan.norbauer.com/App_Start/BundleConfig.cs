﻿using System.Web;
using System.Web.Optimization;

namespace Norbauer.RyanDotNorbauerDotCom
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
                                                        // This method takes an array of strings.
            bundles.Add(new StyleBundle("~/bundles/css").Include("~/Assets/stylesheets/screen.css"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
