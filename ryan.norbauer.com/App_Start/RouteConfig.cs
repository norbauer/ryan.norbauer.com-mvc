﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;

namespace Norbauer.RyanDotNorbauerDotCom
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.LowercaseUrls = true;
            routes.AppendTrailingSlash = false;
            
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "SiteRoot",
                url: "",
                defaults: new { controller = "Pages", action = "Show", slug = "LandingPage" }
            );

            routes.MapRoute(
                name: "Article",
                url: "articles/{slug}",
                defaults: new { Controller = "Articles", Action = "Show" }
            );

            routes.MapRoute(
                name: "Photography",
                url: "photography/{slug}",
                defaults: new { Controller = "Photography", Action = "Show", slug = "about" }
            );

            // The following general controller/action mapping route comes before the Page route. 
            // We want to check if a controller/action exist before dropping for a 404 for a 
            // not-found path requested at the root level (such as "/about", or whatever).
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { id = UrlParameter.Optional }
            );

            // A request for a non-existent path at the root will lead to a 404 error from 
            // within the PagesController (because it inherits from SluggedPageController, which
            // implements the 404 behavior for a not-found slug.)
            routes.MapRoute(
                name: "Page",
                url: "{slug}",
                defaults: new { Controller = "Pages", Action = "Show" }
            );
        }
    }
}