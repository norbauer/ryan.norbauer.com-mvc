﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Norbauer.RyanDotNorbauerDotCom.Controllers
{
    public class ArticlesController : SluggedPageController
    {      
        [OutputCache(Duration = (Int32)3.15569e7)] // one year
        public ActionResult Show(String slug)
        {
            return ViewForAGivenSlugOr404PageIfNotFound(slug);
        }

    }
}