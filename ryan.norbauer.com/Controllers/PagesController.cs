﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Norbauer.RyanDotNorbauerDotCom.Controllers
{
    public class PagesController : SluggedPageController
    {
        [OutputCache(Duration = (Int32)3.15569e7)] // one year
        public ViewResult Show(String slug)
        {
            return ViewForAGivenSlugOr404PageIfNotFound(slug);
        }
    }
}