﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Norbauer.RyanDotNorbauerDotCom.Controllers
{
    [OutputCache(Duration = (Int32)3.15569e7)] // one year
    public class PhotographyController : SluggedPageController
    {
        public ActionResult Index()
        {
            return View("projects");
        }

        public ActionResult Show(String slug)
        {
            return ViewForAGivenSlugOr404PageIfNotFound(slug);
        }
    }
}