﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Norbauer.RyanDotNorbauerDotCom.Controllers
{
    public abstract class SluggedPageController : Controller
    {
        protected ViewResult ViewForAGivenSlugOr404PageIfNotFound(String slug)
        {
            ViewEngineResult resultOfFindOperationForView = ViewEngines.Engines.FindView(ControllerContext, slug, null);

            IView viewTemplate = resultOfFindOperationForView.View;

            if (viewTemplate == null)
            {
                Response.StatusCode = 404;
                return View("Error");
            }
            else
            {
                return View(viewTemplate);
            }
        }
    }
}