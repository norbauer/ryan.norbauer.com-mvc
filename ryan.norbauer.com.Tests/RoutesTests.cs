﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.Routing;
using NSubstitute;
using NUnit.Framework;

namespace ryan.norbauer.com.Tests
{
    [TestFixture]
    public class RoutesTests
    {
        [Test]
        public void Routes_GivenARequestToBareDomain_ShouldRouteToLandingPage()
        {
            AssertRoute("", "Pages", "Show", new { Slug = "LandingPage"} );
        }

        [Test]
        public void Routes_GivenARequestForAnArticle_ShouldRouteItProperly()
        {
            AssertRoute("articles/nougat-de-montelimar", "Articles", "Show", new { Slug = "nougat-de-montelimar" });
        }

        [Test]
        public void Routes_GivenARequestForAPageAtRootLevel_ShouldRouteItProperly()
        {
            AssertRoute("biography", "Pages", "Show", new { Slug = "biography" });
        }

        [Test]
        public void Routes_GivenARequestForARandomControllerAndActionWithId_ShouldRouteItProperly()
        {
            AssertRoute("controllerthing/actionthing/idthing", "controllerthing", "actionthing", new { Id = "idthing" });
        }

        [Test]
        public void Routes_GivenARequestForThePhotographyRoot_ShouldRouteItToTheAboutPage()
        {
            AssertRoute("photography", "Photography", "Show", new { slug = "about" });
            AssertRoute("photography/", "Photography", "Show", new { slug = "about" });
        }

        [Test]
        public void Routes_GivenARequestForAPhotographyPage_ShouldRouteItProperly()
        {
            AssertRoute("photography/some-page", "Photography", "Show", new { slug = "some-page" });
        }

        private void AssertRoute(String pathRelativeToSiteRoot, String expectedController, String expectedAction, Object expectedExtraParameters = null)
        {
            var routes = new RouteCollection();
            Norbauer.RyanDotNorbauerDotCom.RouteConfig.RegisterRoutes(routes);

            var fakeHttpContextToHoldRequestedURL = Substitute.For<HttpContextBase>();
            // GetRouteData requires that path always begin with "~/"
            var path = "~/" + pathRelativeToSiteRoot;
            fakeHttpContextToHoldRequestedURL.Request.AppRelativeCurrentExecutionFilePath.Returns(path);

            RouteData routeData = routes.GetRouteData(fakeHttpContextToHoldRequestedURL);
            Assert.IsNotNull(routeData);

            Assert.AreEqual(expectedController, routeData.Values["Controller"]);
            Assert.AreEqual(expectedAction, routeData.Values["Action"]);

            if (expectedExtraParameters != null)
            {
                foreach (PropertyValue property in GetProperties(expectedExtraParameters))
                {
                    Assert.IsTrue(string.Equals(property.Value.ToString(),
                                                routeData.Values[property.Name].ToString(),
                                                StringComparison.OrdinalIgnoreCase)
                                  , string.Format("Expected '{0}', not '{1}' for '{2}'.",
                                                  property.Value, routeData.Values[property.Name], property.Name));
                }    
            }
        }

        private static IEnumerable<PropertyValue> GetProperties(object o)
        {
            if (o != null)
            {
                PropertyDescriptorCollection props = TypeDescriptor.GetProperties(o);
                foreach (PropertyDescriptor prop in props)
                {
                    object val = prop.GetValue(o);
                    if (val != null)
                    {
                        yield return new PropertyValue { Name = prop.Name, Value = val };
                    }
                }
            }
        }

        private sealed class PropertyValue
        {
            public string Name { get; set; }
            public object Value { get; set; }
        }
    }
}
